# recsync conda recipe

Home: https://github.com/ChannelFinder/recsync

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS Record Synchronizer
